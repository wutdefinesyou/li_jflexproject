// jscanner.jflex
// Zishuo Li

package scanner;
/**
 * Scanner finds the lexeme for words, numbers, identifiers, comments, and some symbols and keywords
 */

%%

%class MyScanner
%function nextToken
%type Token
%eofval{
  return null;
%eofval}
%{
  // instatiate the hash table at the beginning
  LookUpTable table = new LookUpTable();
%}

letter  = [A-Za-z]
word    = {letter}+
number  = [^-?\d+(\.\d+((E|e)(-|\+)?\d+)?)?$]
symbol  = [<][=]|[>][=]|[!][=]|[&][&]|[|][|]|\x21|[\x28-\x2B]|\x2D|\x2F|[\x3B-\x3E]|\x5B|\x5D|\x7B|\x7D
comment = [//.*$]|[/\*+.*\*/]
identifiers = [A-Za-z][A-Za-z0-9]*
whitespace    = [ \n\t]+
other   = .

%%

{identifiers}      {
              Token t = new Token();
              t.lexeme = yytext();
              // matches as a word
              t.type = TokenType.ID;
              // if one of the keywords, reassign its token type
              if (table.containsKey(t.lexeme))
                t.type = (TokenType) table.get(t.lexeme);
              return (t);
            }
{number}    {
              Token t = new Token();
              t.lexeme = yytext();
              t.type = TokenType.NUMBER;
              return (t);
            }
{symbol}    {
              Token t = new Token();
              t.lexeme = yytext();
              // use the table to find the type
              t.type = (TokenType) table.get(t.lexeme);
              return t;
            }
{comment}   {
              /* Do nothing */
            }
{whitespace}  {/* Do nothing */}
{other}   {/* Do nothing */}
