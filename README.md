## Overview

A scanner which returns tokens for identifiers, numbers, and all the keywords and symbols listed.

The scanner is created using the JFlex tool.

## Author

Zishuo Li

## File Listing

```
|--.gitignore
|--README.md
|--jflex.jar
|--jscanner.jflex
|--scanner
    |--Main.java
    |--Token.java
    |--TokenType.java
    |--LookUpTable.java
|--test1.txt
|--test2.txt
```

## How to Run

In your terminal, run ```java -jar jflex.jar jscanner.jflex```. Then run ```javac -d scanner/ MyScanner.java scanner/Main.java```. Run ```java scanner/Main (testingFileName)```.
