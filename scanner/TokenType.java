package scanner;

/**
  * enum of different token types
  */
public enum TokenType{
  NUMBER,
  ID,

  PLUS,
  MINUS,
  MULTIPLY,
  DIVIDE,
  EQUAL,
  LESS,
  GREATER,
  LESSEQUAL,
  GREATEREQUAL,
  NOTEQUAL,
  AND,
  OR,
  EXCLAMATION,
  SEMICOLUMN,
  LEFTPAR,
  RIGHTPAR,
  LEFTBRACKET,
  RIGHTBRACKET,
  LEFTBRACE,
  RIGHTBRACE,
  CHAR,
  INT,
  FLOAT,
  IF,
  ELSE,
  WHILE,
  PRINT,
  RETURN,
  READ,
  RETURN,
  FUNC,
  PROGRAM,
  END,
}
