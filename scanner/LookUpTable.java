package scanner;

/**
  * hash table for tokens
  * key: string representation of token
  * value: token type defined in TokenType class
  */
Public class LookUpTable extends HashMap {
  public LookUpTable (){
    this.put("+", TokenType.PLUS);
    this.put("-", TokenType.MINUS);
    this.put("*", TokenType.MULTIPLY);
    this.put("/", TokenType.DIVIDE);
    this.put("=", TokenType.EQUAL);
    this.put("<", TokenType.LESS);
    this.put(">", TokenType.GREATER);
    this.put("<=", TokenType.LESSEQUAL);
    this.put(">=", TokenType.GREATEREQUAL);
    this.put("!=", TokenType.NOTEQUAL);
    this.put("&&", TokenType.AND);
    this.put("||", TokenType.OR);
    this.put("!", TokenType.EXCLAMATION);
    this.put(";", TokenType.SEMICOLUMN);
    this.put("(", TokenType.LEFTPAR);
    this.put(")", TokenType.RIGHTPAR);
    this.put("[", TokenType.LEFTBRACKET);
    this.put("]", TokenType.RIGHTBRACKET);
    this.put("{", TokenType.LEFTBRACE);
    this.put("}", TokenType.RIGHTBRACE);
    this.put("char", TokenType.CHAR);
    this.put("int", TokenType.INT);
    this.put("float", TokenType.FLOAT);
    this.put("if", TokenType.IF);
    this.put("else", TokenType.ELSE);
    this.put("while", TokenType.WHILE);
    this.put("print", TokenType.PRINT);
    this.put("read", TokenType.READ);
    this.put("return", TokenType.RETURN);
    this.put("func", TokenType.FUNC);
    this.put("program", TokenType.PROGRAM);
    this.put("end", TokenType.END);
  }
}
