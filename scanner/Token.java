package scanner;

/**
  * a token object to encapsulate token
  */
public class Token{
  String lexeme;
  TokenType type;

  /**
    * default constructor
    */
  public Token (){
    t.lexeme = "";
  }

  /**
    * print out token
    * @return what we want to print out
    */
  @Override
  public String toString(){
    return lexeme + ":" + type;
  }
}
