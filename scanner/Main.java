package scanner;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;

/**
  *tests our scanner
  */
public class Main{
  public static void main (String[] args){
    String filename = args[0];
    FileInputStream fis = null;
    try{
      fis = new FileInputStream(filename);
    }
    catch (FileNotFoundException e){
      System.err.println("File not found.");
    }
    InputStreamReader isr = new InputStreamReader(fis);
    MyScanner myscanner = new MyScanner(isr);
    Token result = null;
    try{
      result = myscanner.nextToken();
    }
    catch (Exception e)
    {
      System.err.println("Not a token.");
    }
    while (result != null){
      System.out.println("nextToken() returned " + result);
      try{
        result = myscanner.nextToken();
      }
      catch (Exception e)
      {
        System.err.println("Not a token.");
      }
    }

    // isr.close();
    // fis.close();
    System.out.println("Done!");
  }
}
